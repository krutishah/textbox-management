<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Textbox extends Model
{
    protected $fillable = [
        'name',
    ];
}
