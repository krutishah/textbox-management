@extends('layouts.base')
@section('title',  'Text Management')
@section('main')
<div class="row">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-8">
                <label for="totalTextbox">TextBoxes:</label>
                <input type="text" id="totalTextbox" name="totalTextbox" placeholder="Enter Numeric Value">
                <span class="error">Please enter numeric value</span>
                <button type="button" class="btn btn-primary" id='drawTextBoxes'> Draw</button>
            </div>
        </div>
    </div>
    <div class="col-sm-12 showData" style="display: none">
        <form  name='add_text_form' id='add_text_form' action="{{route('textbox.store')}}" method="POST" onsubmit="return false">

            <h1 class="display-3">Textboxes</h1>
            <div id="textboxList">

            </div>
            <div class="commonErrorMessage" style="display: none">
                <span class="error_show">Please Enter all textbox values</span> 
            </div>
            <h1 class="display-3">Buttons</h1>
            <div id="buttonList">

            </div>
            <h1 class="display-3">Labels</h1>
            <div id="labelList">

            </div>
            <div>
                <button type="submit" value="submit" id="submit"> Submit</button>
                <button type="button" value="refresh" onclick="refreshAll()">Refresh</button>

            </div>
        </form>
    </div>
</div>
@endsection
<style>

    .error{
        display: none;
        margin-left: 10px;
    }		

    .error_show{
        color: red;
        margin-left: 10px;
    }
    .labelColor{
        background-color: yellow;
    }
</style>
@push('after-scripts')
<script>
    var getTextboxList = "{{route('textbox.getList')}}";
    var saveTextboxes = "{{route('textbox.store')}}";
    var totalTextboxes = [];
    var isValid = true;

    $(document).ready(function ()
    {
        $("#drawTextBoxes").click(function () {
            totalTextboxes = [];
            $("#textboxList").html('');

            var totalValue = $('#totalTextbox').val();
            var is_numeric = $.isNumeric(totalValue);
            if (is_numeric) {
                $(".showData").show();
                $('.error').hide();
            } else {
                $(".showData").hide();
                $('.error').show();
            }

            for ($i = 0; $i < totalValue; $i++) {

                totalTextboxes.push({'id': $i, name: "textbox" + $i, label: "textbox" + $i});
                $("#textboxList").append("<input type='text' onchange='changeLabel(" + $i + ")' class='mr-2' name='textbox[" + $i + "]' id='textbox" + $i + "' value='textbox" + $i + "'> ");
            }
            shuffle(totalTextboxes);





        });








        function getList() {
            $.ajax({
                url: getTextboxList,
                type: 'GET',
                async: true,
                dataType: 'json',
                success: function (response) {
                    $("#textboxList").html('');
                    $("#totalTextbox").val(response.length);
                    if (response.length > 0) {
                        $(".showData").show();
                        $.each(response, function (index, value) {
                            totalTextboxes.push({'id': index, name: "textbox" + index, label: value.name});
                            $("#textboxList").append("<input type='text' onchange='changeLabel(" + index + ")' class='mr-2' name='textbox[" + index + "]' id='textbox" + index + "' value='" + value.name + "'> ");
                            $("#buttonList").append("<button type='button' onclick='changeLabelColor(" + index + ")' id='buttonList" + index + "' class='btn btn-primary'>" + value.name + " </button>");
                        });
                        shuffle(totalTextboxes);
                    } else {
                        $(".showData").hide();
                    }

                }
            })

        }
        getList();

    });



    function shuffle(array) {
        $("#labelList").html('');
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        $.each(array, function (index, value) {
            $("#labelList").append("<label class='labelClass' id='button" + value.id + "'>" + value.label + " </label>");

        });
    }



    function changeLabel(id = 0) {
        var label = $("#textbox" + id).val();
        if (label == '' || label == null) {
            isValid = false;
            $(".commonErrorMessage").show();
        } else {
            $("#button" + id).html(label);
            $("#buttonList" + id).html(label);
        }
        $.each(totalTextboxes, function (index, value) {
            if (value.id == id) {
                totalTextboxes[index].name = label;
                totalTextboxes[index].label = label;
            }

        });
    }


    function refreshAll() {
        shuffle(totalTextboxes);
    }
    
    function changeLabelColor(id = 0) {
        alert(id);
    }


    $("#submit").click(function (event) {
        event.preventDefault();
        var form_data = $("#add_text_form").serializeArray();
        isValid = true;
        $.each($('[name^=textbox]'), function (index, value) {
            if (isValid) {
                if ($("#" + $(value).attr('id')).val() == '') {
                    isValid = false;
                    $(".commonErrorMessage").show();
                }
            }


        });
        if (isValid) {
            $(".commonErrorMessage").hide();
            $("#add_text_form").submit();
            $.ajax({
                url: saveTextboxes,
                type: 'POST',
                async: true,
                dataType: 'json',
                data: {textValue: form_data, "_token": "{{ csrf_token() }}"},
                success: function (response) {

                }
            })
        }

    });

</script>
@endpush