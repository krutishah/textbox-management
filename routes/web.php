<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TextboxController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('textboxes', 'TextboxController@index');
Route::get('/getData', [App\Http\Controllers\TextboxController::class, 'getData'])->name('textbox.getList');
Route::post('store', [App\Http\Controllers\TextboxController::class, 'storeData'])->name('textbox.store');
//Route::post('/createData', [App\Http\Controllers\TextboxController::class, 'getData'])->name('textbox.store');
